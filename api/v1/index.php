<?php
header('Content-Type: text/html; charset=UTF-8');

require_once '../include/DbHandler.php';
require_once '../include/PassHash.php';
require '.././libs/Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

// User id from db - Global Variable
$user_id = NULL;

/**
 * Adding Middle Layer to authenticate every request
 * Checking if the request has valid api key in the 'Authorization' header
 */
function authenticate(\Slim\Route $route)
{
    $headers = apache_request_headers();
    $response = array();
    $app = \Slim\Slim::getInstance();
    if (isset($headers['x-vvv-token'])) {
        $db = new DbHandler();
        $token = $headers['x-vvv-token'];
        if (!$db->isValidToken($token)) {
            $response["error"] = true;
            $response["message"] = "Доступ закрыт. Недопустимый token";
            echoRespnse(401, $response);
            $app->stop();
        } else {
            global $user_id;
            $user = $db->getUserId($token);
            if ($user != NULL)
                $user_id = $user;
        }
    } else {
        $response["error"] = true;
        $response["message"] = "Отсутствует token";
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////GET TOKEN
 */

$app->post('/get_token', function () use ($app) {
    $headers = apache_request_headers();
    $response = array();
    if (isset($headers['x-vvv-secret']) && $headers['x-vvv-secret'] == "G+KbPdSgVkYp3s6v9y!B&E)H@McQfThWmZq4t7w!z%C*F-JaNdRgUkXn2r5u8x/A") {
        //init json
        $json = $app->request->getBody();
        $data = json_decode($json, true);
        //init params
        $email = $data["email"];
        $system_from = $data["system_from"];
        $password = $data["password"];
        $registration = $data["registration"];
        //init db
        $db = new DbHandler();
        //step 1
        $is_email_has = $db->isEmailHas($email);
        if ($is_email_has && $registration) {
            $response["error"] = true;
            $response["message"] = "Извините, этот адрес e-mail уже занят другим пользователем";
            echoRespnse(200, $response);
            return;
        }
        //step 2
        $token = NULL;
        if (!$is_email_has) {
            $token = $db->createUser($email, $password, $system_from);
        } elseif ($is_email_has && !$registration) {
            $token = $db->getToken($email, $password, $system_from);
            if ($token == null) {
                $response["error"] = true;
                if ($system_from == "email") {
                    $response["message"] = "Пожалуйста, проверьте правильность написания email или пароля";
                } else
                    $response["message"] = "Ошибка,попробуйте выбрать другой способ входа";
                echoRespnse(200, $response);
                return;
            }
        }
        //step3

        if ($token != NULL) {
            $need_user_data = $db->isNeedUserData($token);
            $data = array();
            $data["token"] = $token;
            $data["need_user_data"] = $need_user_data;

            $response["error"] = false;
            $response["message"] = "Вход выполнен успешно";
            $response["data"] = $data;
            echoRespnse(201, $response);
        } else {
            $response["error"] = true;
            $response["message"] = "Ошибка,попробуйте выбрать другой способ входа";
            echoRespnse(200, $response);
        }
    } else {
        $response["error"] = true;
        $response["message"] = "Ошибка, свяжитесь со службой технической поддержки vvvteam.ru";
        echoRespnse(200, $response);
    }
});


$app->post('/update_user', 'authenticate', function () use ($app) {
    $response = array();
    global $user_id;
    $json = $app->request->getBody();
    $data = json_decode($json, true);
    //init params
    $name = $data["name"];
    $date_of_birth = $data["date_of_birth"];
    $sex = $data["sex"];
    $photo = $data["photo"];
    //init db
    $db = new DbHandler();
    $status = $db->updateUser($user_id, $name, $date_of_birth, $sex, $photo);
    if ($status) {
        $response["error"] = false;
        $response["message"] = "Данные успешно обновлены";
    } else {
        $response["error"] = true;
        $response["message"] = "Ошибка в обновлении данных пользователя";
    }
    echoRespnse(201, $response);
});

/*
$app->get('/category', 'authenticate', function() use ($app) {
            verifyRequiredParams(array('type'));
			
			$type = $app->request->get('type');
            
			$response = array();
            
			$db = new DbHandler();
			$result = $db->getCategory($type);
 				
			$response["error"] = false;
			$response["message"] = "Category " . $type;
			$response["data"] = array();
				
			while ($category = $result->fetch_assoc()) {
				 $tmp = array();
				$tmp["id"] = $category["id"];
                $tmp["name"] = $category["name_category"];
                $tmp["image"] = $category["image"];
                array_push($response["data"], $tmp); 
			}
			echoRespnse(200, $response);
			 
        });
		
   
$app->get('/category_sport', 'authenticate', function() use ($app) {
            verifyRequiredParams(array('id'));
			
			$id = $app->request->get('id');
           
			$response = array();
            global $user_id;
			$db = new DbHandler();
			$result = $db->getCategorySportById($id,$user_id);
 				
			$response["error"] = false;
			$response["message"] = "Category sport";
			$response["data"] = array();
				
			while ($category = $result->fetch_assoc()) {
				$imgtmp["images"] = array();
			    $resultimg = $db->getSportImageById($category["id"]);
				while ($img = $resultimg->fetch_assoc()) {
					$tmpim = array();
					$tmpim["id"] = $img["id"];
					$tmpim["image"] = $img["res_name"];
					array_push($imgtmp["images"], $tmpim); 
				}
				$tmp = array();
				$tmp["id"] = $category["id"];
                $tmp["name"] = $category["name"];
                $tmp["image"] = $category["image"];
				$tmp["description"] = $category["description"];
				$tmp["param"] = $category["params"];
				$tmp["images"] = $imgtmp["images"];
				$tmp["like"] = $category["w_like"] === 1? true: false;
				$tmp["advice"] = $category["advice"];
                array_push($response["data"], $tmp); 
			}
			echoRespnse(200, $response);
			 
        });
		
		
$app->get('/category_food', 'authenticate', function() use ($app) {
            verifyRequiredParams(array('id'));
			
			$id = $app->request->get('id');
           
			$response = array();
            global $user_id;
			$db = new DbHandler();
			$result = $db->getCategoryFoodById($id, $user_id);
 				
			$response["error"] = false;
			$response["message"] = "Category food";
			$response["data"] = array();
				
			while ($category = $result->fetch_assoc()) {
				$tmp = array();
				$tmp["id"] = $category["id"];
                $tmp["name"] = $category["name"];
                $tmp["image"] = $category["image"];
				$tmp["description"] = $category["description"];				
				$tmp["proteins"] = $category["proteins"];
                $tmp["fats"] = $category["fats"];
				$tmp["carbohydrates"] = $category["carbohydrates"];
				$tmp["minerals"] = $category["minerals"];
				$tmp["vitamins"] = $category["vitamins"];
				$tmp["calories"] = $category["calories"];
				$tmp["like"] = $category["w_like"] === 1? true: false;
				array_push($response["data"], $tmp); 
			}
			echoRespnse(200, $response);
			 
        });
		
		
$app->post('/like', 'authenticate', function() use ($app) {  

            $response = array();
			$json = $app->request->getBody();
			$data = json_decode($json, true); // parse the JSON into an assoc. array
			
            global $user_id;
            $db = new DbHandler();
 
		 	foreach ($data as $tmp) {
				$type = $tmp["type"];
				$like = $tmp["like"];
				$id_item = $tmp["idItem"];
				$status = $db->setLike($user_id, $id_item, $like, $type); 
				if(!$status) { break;}
			}
			
            if ($status) {
                $response["error"] = false;
                $response["message"] = "All rows save";
            } else {
                $response["error"] = true;
                $response["message"] = "Failed save";
            }
            echoRespnse(201, $response);
        });
		
		
$app->post('/set_task', 'authenticate', function() use ($app) {  

            $response = array();
			$json = $app->request->getBody();
			$data = json_decode($json, true); // parse the JSON into an assoc. array
			
            global $user_id;
            $db = new DbHandler();
 
		 	foreach ($data as $tmp) {
				$id = $tmp["id"];
				$name = $tmp["name"];
				$icon_color = $tmp["iconColor"];
				$status = $db->setTasks($user_id, $id, $name, $icon_color); 
				if(!$status) { break;}
			}
			
            if ($status) {
                $response["error"] = false;
                $response["message"] = "All tasks save";
            } else {
                $response["error"] = true;
                $response["message"] = "Failed save";
            }
            echoRespnse(201, $response);
        });
		
		
$app->get('/get_task', 'authenticate', function() use ($app) {       
			
			$response = array();
			
            global $user_id;
			$db = new DbHandler();
			$result = $db->getTasks($user_id);
 				
			$response["error"] = false;
			$response["message"] = "Tasks ";
			$response["data"] = array();
				
			while ($tasks = $result->fetch_assoc()) {
				$tmp = array();
				$tmp["id"] = $tasks["id_user_task"];
                $tmp["name"] = $tasks["name"];
                $tmp["iconColor"] = $tasks["icon_color"];
				$tmp["owner"] = false;
                array_push($response["data"], $tmp); 
			}
			echoRespnse(200, $response);
			 
        });
		
		
$app->get('/template_sport_list', 'authenticate', function() use ($app) { //
            verifyRequiredParams(array('id'));
			
			$id = $app->request->get('id');
           
			$response = array();
            global $user_id;
			$db = new DbHandler();
			$result = $db->getTemplateSport($id, $user_id);//
 				
			$response["error"] = false;
			$response["message"] = "Template sport";
			$response["data"] = array();
				
			while ($category = $result->fetch_assoc()) {
				$tmp = array();
				$tmp["id"] = $category["id"];
                $tmp["name"] = $category["name"];
                $tmp["description"] = $category["description"];
				$tmp["image"] = $category["image"];				
				$tmp["target"] = $category["target"];
                $tmp["sex"] = $category["for_whom"];
				$tmp["week"] = $category["week"];
				$tmp["dayOfWeek"] = $category["day_of_week"];
				$tmp["typeTemplate"] = $category["type_template"];
				$tmp["like"] = $category["w_like"] === 1? true: false;
				
				$days["days"] = array();
			    $resdays = $db->getDaysSportTemplate($tmp["id"]);
				while ($day = $resdays->fetch_assoc()) {
					$tmpdd = array();
					$tmpdd["id"] = $day["id"];
					$tmpdd["idTemplate"] = $day["id_template"];
					$tmpdd["day"] = $day["day"];
					$tmpdd["description"] = $day["description"];
					$tmpdd["status"] = $day["status"]=== 1? true: false;
					$items["items"] = array();
					$resitems = $db->getDaysSportTemplateItems($tmp["id"], $tmpdd["id"]);
					while ($item = $resitems->fetch_assoc()) {
						$tmpitem = array();
						$tmpitem["id"] = $item["id"];
						$tmpitem["idTemplate"] = $item["id_template"];
						$tmpitem["idDay"] = $item["id_day"];
						$tmpitem["idItem"] = $item["id_item"];
						$tmpitem["approach"] = $item["approach"];
						$tmpitem["repetitions"] = $item["repetitions"];
						$tmpitem["restBetweenApproaches"] = $item["rest_between_approaches"];
						$tmpitem["description"] = $item["description"];				
						$ressport = $db->getCategorySportByIdUnic($item["id_item"]);
						while ($spo = $ressport->fetch_assoc()) {
							$imgtmp["images"] = array();
							$resultimg = $db->getSportImageById($spo["id"]);
							while ($img = $resultimg->fetch_assoc()) {
								$tmpim = array();
								$tmpim["id"] = $img["id"];
								$tmpim["image"] = $img["res_name"];
								array_push($imgtmp["images"], $tmpim); 
							}
							$sport["id"] = $spo["id"];
							$sport["name"] = $spo["name"];
							$sport["image"] = $spo["image"];
							$sport["description"] = $spo["description"];
							$sport["param"] = $spo["params"];
							$sport["images"] = $imgtmp["images"];
							$sport["idCategory"] = $spo["id_category"];
							$sport["advice"] = $spo["advice"];
							$tmpitem["sport"] = $sport;
						}
						array_push($items["items"], $tmpitem); 
					}
					$tmpdd["items"] = $items["items"]; 
					array_push($days["days"], $tmpdd); 
				}
				$tmp["days"] = $days["days"];
				array_push($response["data"], $tmp); 
			}
			echoRespnse(200, $response);
			 
        });
		
		
		
				
$app->get('/template_food_list', 'authenticate', function() use ($app) { //
            verifyRequiredParams(array('id'));
			
			$id = $app->request->get('id');
           
			$response = array();
            global $user_id;
			$db = new DbHandler();
			$result = $db->getTemplateFood($id, $user_id);//
 				
			$response["error"] = false;
			$response["message"] = "Template food";
			$response["data"] = array();
		
			while ($category = $result->fetch_assoc()) {
				$tmp = array();
				$tmp["id"] = $category["id"];
                $tmp["name"] = $category["name"];
                $tmp["description"] = $category["description"];
				$tmp["image"] = $category["image"];				
				$tmp["target"] = $category["target"];
                $tmp["sex"] = $category["sex"];
				$tmp["countMeals"] = $category["count_meals"];
				$tmp["like"] = $category["w_like"] === 1? true: false;
				
				$list["list"] = array();
			    $resdays = $db->getFoodTemplateMeals($tmp["id"]);
				while ($meals = $resdays->fetch_assoc()) {
					$tmpdd = array();
					$tmpdd["id"] = $meals["id"];
					$tmpdd["idTemplate"] = $meals["id_template"];
					$tmpdd["idItem"] = $meals["id_item"];
					$tmpdd["meals"] = $meals["meals"];
					$tmpdd["gram"] = $meals["gram"];
				 	$tmpdd["description"] = $meals["description"];
				 	$tmpdd["time"] = $meals["time"];
					
					$resultff = $db->getFoodUnic($meals["id_item"]);
						while ($food = $resultff->fetch_assoc()) {
							$tmpff["id"] = $food["id"];
							$tmpff["idCategory"] = $food["id_category"];
							$tmpff["name"] = $food["name"];
							$tmpff["image"] = $food["image"];
							$tmpff["description"] = $food["description"];				
							$tmpff["proteins"] = $food["proteins"];
							$tmpff["fats"] = $food["fats"];
							$tmpff["carbohydrates"] = $food["carbohydrates"];
							$tmpff["minerals"] = $food["minerals"];
							$tmpff["vitamins"] = $food["vitamins"];
							$tmpff["calories"] = $food["calories"];
							$tmpdd["food"] = $tmpff;
						}
					array_push($list["list"], $tmpdd); 
				}
				$tmp["list"] = $list["list"];
		
				array_push($response["data"], $tmp); 		
			} 
			echoRespnse(200, $response);

        });
	*/
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////OTHER FUNCTION
/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields)
{
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * Validating email address
 */
function validateEmail($email)
{
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Недействительный адрес электронной почты';
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response)
{
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);

    // setting response content type to json
    $app->contentType('application/json');

    echo json_encode($response);
}

$app->run();
?>