<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Ravi Tamada
 * @link URL Tutorial link
 */
class DbHandler
{

    private $conn;

    function __construct()
    {
        require_once dirname(__FILE__) . '/DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

    public function createUser($email, $password, $system_from)
    {
        $token = $this->generateToken();
        $stmt = $this->conn->prepare("CALL CREATE_USER(?,?,?,?)");
        $stmt->bind_param("ssss", $email, $password, $system_from, $token);
        if ($stmt->execute()) {
            $stmt->close();
            return $token;
        } else
            return NULL;
    }

    public function updatePushPoken($user_id, $push_token)
    {
        $stmt = $this->conn->prepare("CALL UPDATE_PUSH_TOKEN(?,?)");
        $stmt->bind_param("is", $user_id, $push_token);
        if ($stmt->execute()) {
            $stmt->close();
            return true;
        } else
            return false;
    }

    public function updateUser($user_id, $name, $date_of_birth, $sex, $photo)
    {
        $stmt = $this->conn->prepare("CALL UPDATE_USER(?,?,?,?,?)");
        $stmt->bind_param("issss", $user_id, $name, $date_of_birth, $sex, $photo);
        if ($stmt->execute()) {
            $stmt->close();
            return true;
        } else
            return false;
    }

    public function isEmailHas($email)
    {
        $result = 0;
        $stmt = $this->conn->prepare("SELECT CHECK_EMAIL(?) as result");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $result = 0;
        $res = $stmt->get_result();
        while ($row = $res->fetch_assoc()) {
            $result = $row['result'];
        }
        $stmt->close();
        return $result == 0 ? false : true;
    }

    public function isNeedUserData($token)
    {
        $stmt = $this->conn->prepare("SELECT NEED_USER_DATA(?) as result");
        $stmt->bind_param("s", $token);
        $stmt->execute();
        $result = 1;
        $res = $stmt->get_result();
        while ($row = $res->fetch_assoc()) {
            $result = $row['result'];
        }
        $stmt->close();
        return $result == 0 ? false : true;
    }

    public function getUserId($token)
    {
        $stmt = $this->conn->prepare("SELECT GET_USER_ID(?) as result");
        $stmt->bind_param("s", $token);
        $stmt->execute();
        $result = 0;
        $res = $stmt->get_result();
        while ($row = $res->fetch_assoc()) {
            $result = $row['result'];
        }
        $stmt->close();
        return $result;
    }

    public function getToken($email, $password, $system_from)
    {
        $stmt = $this->conn->prepare("SELECT GET_TOKEN(?,?,?) as result");
        $stmt->bind_param("sss", $email, $password, $system_from);
        $stmt->execute();
        $result = null;
        $res = $stmt->get_result();
        while ($row = $res->fetch_assoc()) {
            $result = $row['result'];
        }
        $stmt->close();
        return $result;
    }


    private function generateToken()
    {
        return md5(uniqid(rand(), true));
    }

    public function isValidToken($token)
    {
        $stmt = $this->conn->prepare("SELECT IS_VALID_TOKEN(?) as result");
        $stmt->bind_param("s", $token);
        $stmt->execute();
        $result = 0;
        $res = $stmt->get_result();
        while ($row = $res->fetch_assoc()) {
            $result = $row['result'];
        }
        $stmt->close();
        return $result == 0 ? false : true;
    }
    /*
        public function getApiKey($email, $password, $android_id, $type) {
            if($this->isUserInApp($email, $password, $type))
            {
                return $this->getApiKeyBySystemToken($email, $password, $type);
            } else
            {
                return $this->createNewUser($name, $email, $type_sign, $system_token);
            }
        }

        private function isUserInApp($email, $password, $type) {
            $stmt = $this->conn->prepare("SELECT id from user_details WHERE password = ? and type_sign = ? and email = ?");
            $stmt->bind_param("ss", $password, $type, $email);
            $stmt->execute();
            $stmt->store_result();
            $num_rows = $stmt->num_rows;
            $stmt->close();
            return $num_rows > 0;
        }

        private function getApiKeyBySystemToken($email, $password, $type) {
            $stmt = $this->conn->prepare("SELECT token from user_details WHERE email = ? and password = ? and type_sign = ?");
            $stmt->bind_param("sss", $email, $password, $type);
             if ($stmt->execute()) {
                 //$stmt->bind_result($api_key);
                 $api_key = $stmt->get_result()->fetch_assoc();
                 $stmt->close();
                return $api_key;
            } else {
                return NULL;
           }
        }

        private function createNewUser($type_sign, $email, $password, $android_id)
        {
            $api_key = $this->generateApiKey();
            $stmt = $this->conn->prepare("CALL create_user_details(?,?,?,?,?)");
            $stmt->bind_param("sssss",$type_sign, $email, $password, $api_key , $android_id);
            $response = array();
            $response["api_key"] = $api_key;
            if($stmt->execute()) {
                $stmt->close();
                return $response;
            } else
                return false;
        }


        private function generateApiKey() {
            return md5(uniqid(rand(), true));
        }

           private function isNeedUserData($token) {
            $stmt = $this->conn->prepare("SELECT id from user_details WHERE password = ? and type_sign = ? and email = ?");
            $stmt->bind_param("s", $token);
            $stmt->execute();
            $stmt->store_result();
            $num_rows = $stmt->num_rows;
            $stmt->close();
            return $num_rows > 0;
        }


    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////CATEGORY
        public function getCategory($type) {
        $stmt = $this->conn->prepare("SELECT id, name_category, type, image FROM category WHERE type = ?");
        $stmt->bind_param("s",$type);
        $stmt->execute();
        $category = $stmt->get_result();
        $stmt->close();
        return $category;

        }

        public function isValidApiKey($api_key) {
            $stmt = $this->conn->prepare("SELECT id from users WHERE api_key = ?");
            $stmt->bind_param("s", $api_key);
            $stmt->execute();
            $stmt->store_result();
            $num_rows = $stmt->num_rows;
            $stmt->close();
            return $num_rows > 0;
        }

        public function getCategorySportById($id, $user) {
        $stmt = $this->conn->prepare("    SELECT
            `x`.`id` AS `id`,
            `x`.`id_category` AS `id_category`,
            `x`.`name` AS `name`,
            `x`.`description` AS `description`,
            `x`.`params` AS `params`,
            `x`.`image` AS `image`,
            IFNULL(`u`.`w_like`, 0) AS `w_like`,
            `u`.`id_user` AS `id_user`,
            x.advice
        FROM
            (`admin_st`.`sport` `x`
            LEFT JOIN `admin_st`.`user_likes` `u` ON (((`x`.`id` = `u`.`id_item`)
                AND (`u`.`w_type` = 'sport')) and id_user = ?))
        where id_category = ?");
        $stmt->bind_param("ii", $user,$id);
        $stmt->execute();
        $category = $stmt->get_result();
        $stmt->close();
        return $category;
        }

        public function getCategoryFoodById($id, $user) {
        $stmt = $this->conn->prepare("    SELECT
            `x`.`id` AS `id`,
            `x`.`id_category` AS `id_category`,
            `x`.`name` AS `name`,
            `x`.`description` AS `description`,
            `x`.`image` AS `image`,
            `x`.`proteins` AS `proteins`,
            `x`.`fats` AS `fats`,
            `x`.`carbohydrates` AS `carbohydrates`,
            `x`.`minerals` AS `minerals`,
            `x`.`vitamins` AS `vitamins`,
            `x`.`calories` AS `calories`,
            IFNULL(`u`.`w_like`, 0) AS `w_like`,
            `u`.`id_user` AS `id_user`
        FROM
            (`admin_st`.`food` `x`
            LEFT JOIN `admin_st`.`user_likes` `u` ON ((`x`.`id` = `u`.`id_item`)
                AND (`u`.`w_type` = 'food')  and id_user = ?))
        where id_category = ?");
        $stmt->bind_param("ii", $user,$id);
        $stmt->execute();
        $category = $stmt->get_result();
        $stmt->close();
        return $category;
        }

        public function getTemplateSport($id, $user) {
        $stmt = $this->conn->prepare("SELECT
            x.id,
            x.id_category,
            x.name,
            x.description,
            x.image,
            x.target,
            x.for_whom,
            x.week,
            x.day_of_week,
            x.type_template,
            IFNULL(`u`.`w_like`, 0) AS `w_like`,
            `u`.`id_user` AS `id_user`
        FROM
            (`admin_st`.`sport_template` `x`
            LEFT JOIN `admin_st`.`user_likes` `u` ON ((`x`.`id` = `u`.`id_item`)
                AND (`u`.`w_type` = 'sport_template')  and id_user = ?))
        where id_category = ?");
        $stmt->bind_param("ii", $user,$id);
        $stmt->execute();
        $category = $stmt->get_result();
        $stmt->close();
        return $category;
        }


        public function getTemplateFood($id, $user) {
        $stmt = $this->conn->prepare("SELECT
            x.id,
            x.id_category,
            x.name,
            x.description,
            x.image,
            x.target,
            x.sex,
            x.count_meals,
            IFNULL(u.w_like, 0) AS w_like,
            u.id_user AS id_user
        FROM
            (admin_st.food_template x
            LEFT JOIN admin_st.user_likes u ON ((x.id = u.id_item)
                AND (u.w_type = 'food_template')  and id_user = ?))
        where id_category = ?");
        $stmt->bind_param("ii", $user,$id);
        $stmt->execute();
        $category = $stmt->get_result();
        $stmt->close();
        return $category;
        }


        public function getSportImageById($id) {
        $stmt = $this->conn->prepare("SELECT * FROM sport_img WHERE id_exercise = ?");
        $stmt->bind_param("i",$id);
        $stmt->execute();
        $category = $stmt->get_result();
        $stmt->close();
        return $category;
        }

        public function getDaysSportTemplate($id) {
        $stmt = $this->conn->prepare("SELECT * FROM sport_template_day WHERE id_template = ?");
        $stmt->bind_param("i",$id);
        $stmt->execute();
        $templatesport = $stmt->get_result();
        $stmt->close();
        return $templatesport;
        }

        public function getFoodTemplateMeals($id) {
        $stmt = $this->conn->prepare("SELECT * FROM admin_st.food_template_list WHERE id_template = ?");
        $stmt->bind_param("i",$id);
        $stmt->execute();
        $templatesport = $stmt->get_result();
        $stmt->close();
        return $templatesport;
        }

        public function getDaysSportTemplateItems($id_template, $id_day) {
        $stmt = $this->conn->prepare("SELECT * FROM admin_st.sport_template_list where id_template = ? and id_day = ?");
        $stmt->bind_param("ii",$id_template, $id_day);
        $stmt->execute();
        $items = $stmt->get_result();
        $stmt->close();
        return $items;
        }



        public function getCategorySportByIdUnic($id) {
        $stmt = $this->conn->prepare("SELECT * FROM  admin_st.sport   where id = ?");
        $stmt->bind_param("i",$id);
        $stmt->execute();
        $category = $stmt->get_result();
        $stmt->close();
        return $category;
        }

        public function getFoodUnic($id) {
        $stmt = $this->conn->prepare("SELECT * FROM  admin_st.food where id = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $category = $stmt->get_result();
        $stmt->close();
        return $category;
        }


        public function setLike($user, $id_item, $like, $type) {
        $stmt = $this->conn->prepare("CALL set_like(?,?,?,?)");
        $stmt->bind_param("iiis",$user, $id_item, $like, $type);
        //$result = $stmt->execute();
        if($stmt->execute()) {
            $stmt->close();
            return true;
        } else
            return false;
        }

        public function setTasks($user, $id, $name, $icon_color) {
        $stmt = $this->conn->prepare("CALL set_task(?,?,?,?)");
        $stmt->bind_param("iisi",$user, $id, $name, $icon_color);
        if($stmt->execute()) {
            $stmt->close();
            return true;
        } else
            return false;
        }


        public function getTasks($user) {
        $stmt = $this->conn->prepare("SELECT * FROM  admin_st.user_tasks where id_user = ?");
        $stmt->bind_param("i",$user);
        $stmt->execute();
        $category = $stmt->get_result();
        $stmt->close();
        return $category;

        }

        public function getUserId($api_key) {
            $stmt = $this->conn->prepare("SELECT id FROM users WHERE api_key = ?");
            $stmt->bind_param("s", $api_key);
            if ($stmt->execute()) {
                $user_id = $stmt->get_result()->fetch_assoc();
                $stmt->close();
                return $user_id;
            } else {
                return NULL;
            }
        }*/
}

?>
